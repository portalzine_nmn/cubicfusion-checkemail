
cubicFUSION checkEmail
=====================================
* * *

http://www.portalzine.de

About
-----

PHP Class to verify emails in one brief sweep. Minimal configuration needed.
- Whitelist & Blacklist check
- User bad word filter check
- Verify DNS
- Verify email over SMTP
- Check domain ip against Spam blacklists

License
-----------

/LICENSE.txt.